const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const base64 = require('js-base64').Base64;
const stringify = require('csv-stringify');
const appconfig = require('./config.json');
const readFile = require('fs').readFile;
const axios = require('axios');
const ejs = require('ejs');
const CSVToJSON = require("csvtojson");
const JSONToCSV = require("json2csv").parse;
const multer = require('multer')
const upload = multer({
    dest: 'upload/'
    //storage: multer.memoryStorage()
})
const fs = require('fs');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const app = express();
var APIoperations = require('./APIcalls.js');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); //changed pug to ejs

app.get('/', (req, res) => {
    res.render('pages/LoginPage', {
        LoginFail: ""
    });
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

//Cookie will be signed using this secret
app.use(cookieParser(appconfig.Values.CookieSecret));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

///----Global Variables-----
var JobNumber;
var objtoPOST = {
    Token: "",
    EntityType: 3,
    JobType: 1, //Job Type 1 is post//Job Type 2 is patch
    data: [
        /*  EstHours:"",
        JobID: "",
        Month: "",
        UserID: "" */
    ]
};
//-----------------------------
app.post('/UploadPage', (req, res) => {
    console.log("Config"+appconfig.Values.MonthlyHours)
    res.render('pages/UploadPage', {
        abcde: appconfig.Values.MonthlyHours
    });
})
app.post('/DownloadPage', (req, res) => {
    res.render('pages/DownloadPage',{
        gtoken: req.signedCookies.AuthTokenCookie
    } );
})
app.post('/UpdatePage', (req, res) => {
    res.render('pages/UpdatePage',{
        gtoken: req.signedCookies.AuthTokenCookie
    } );
})
app.post('/DeletePage', (req, res) => {
    res.render('pages/DeletePage',{
        gtoken: req.signedCookies.AuthTokenCookie
    } );
})


app.post('/home', (req, res) => {
    let email = req.body.Email;
    let password = req.body.Password;
    const encodedToken = base64.encode(`${email}:${password}`);
    const basic = 'Basic ';
    token = `${basic}${encodedToken}`;

    var loginDetail = APIoperations.getLoginDetail(token).then(function(response) {
            // handle success
            console.log("Successful Login");
            
            //save AuthToken in cookies
            res.cookie('AuthTokenCookie', response.data.data.AuthToken.Token, {
                signed: true,
                httpOnly: true
            });

            res.render('pages/UploadPage', {
                global_token: response.data.data.AuthToken.Token
            });
        })
        .catch(function(error) {
            // handle error
            res.render('pages/LoginPage', {
                LoginFail: "Invalid Login"
            });
        })

})

app.post('/logout', (req, res) => {
    
    res.cookie('AuthTokenCookie', "", {
        maxAge: 0
    });
    res.render('pages/LoginPage',{
        LoginFail: ""
    });

})


app.post('/statuspage', (req, res) => {

    var aaa = APIoperations.getJobList(req.signedCookies.AuthTokenCookie).then(function(response) {
            // handle success
            //table rendered in StatusPage ejs file
            console.log("Got Jobs List");
            console.log(response.data);
            
            res.render('pages/StatusPage', {
                htmltable: response.data
            });

        })
        .catch(function(error) {
            // handle error
            res.send('Error getting Jobs List, please try again');

        })
})

//--Return home from status page
app.post('/returnhome', (req, res) => {
    res.render('pages/UploadPage', {
        global_token: req.signedCookies.AuthTokenCookie
    });

})

//Express post to receive the csv file submitted by user in the browser
app.post('/upload', upload.single('filename'), (req, res) => {
    console.log("Upload Submitting...")

    //Reset objtoPOST data for the next upload
    objtoPOST.data = []; 
    //Set EntityType based on client's selection in dropdown
    objtoPOST.EntityType = req.body.EntityTypeID;
    //JobType 1 is Update
    objtoPOST.JobType = 1;
    objtoPOST.Token = req.signedCookies.AuthTokenCookie;
   

    console.log("CSV2JSON---now!")
    //Convert CSV file to JSON format
    CSVToJSON().fromFile("./" + req.file.path).then(source => {
        
        for (var csventry in source) { //Loop through each excel row
            var toPush = { //this object will be pushed into objtoPOST.data each loop
                /*  EstHours:"",
                        JobID: "", Obtained using speekdey
                        Month: "",
                        UserID: "" Obtained using Employee No*/
            }
            if(req.body.EntityTypeID==1){
                toPush.AccountingPackageID = source[csventry]["AccountingPackageID"];
                toPush.AllowIncompleteTimesheetSubmission = source[csventry]["AllowIncompleteTimesheetSubmission"];
                toPush.BillingRate = source[csventry]["BillingRate"];
                toPush.CostRate = source[csventry]["CostRate"];
                toPush.EmployeeNumber = source[csventry]["EmployeeNumber"];
                toPush.SecurityLevel = source[csventry]["SecurityLevel"];
                toPush.MinimumTimeHours = source[csventry]["MinimumTimeHours"];
                toPush.MinimumTimePeriod = source[csventry]["MinimumTimePeriod"];
                toPush.IsActive = source[csventry]["IsActive"];
                toPush.StartDate = source[csventry]["StartDate"];
                toPush.EndDate = source[csventry]["EndDate"];
            }
            else if(req.body.EntityTypeID==2){
                toPush.Name = source[csventry]["Name"];
                toPush.JobNumber = source[csventry]["JobNumber"];
                toPush.BillingRate = source[csventry]["BillingRate"];
                toPush.Notes = source[csventry]["Notes"];
                toPush.IsActive = source[csventry]["IsActive"];
            }
            else if(req.body.EntityTypeID==3){
                toPush.AccountingPackageID = source[csventry]["AccountingPackageID"];
                toPush.AllowIncompleteTimesheetSubmission = source[csventry]["AllowIncompleteTimesheetSubmission"];
                toPush.BillingRate = source[csventry]["BillingRate"];
                toPush.CostRate = source[csventry]["CostRate"];
                toPush.EmployeeNumber = source[csventry]["EmployeeNumber"];
                toPush.SecurityLevel = source[csventry]["SecurityLevel"];
                toPush.MinimumTimeHours = source[csventry]["MinimumTimeHours"];
                toPush.MinimumTimePeriod = source[csventry]["MinimumTimePeriod"];
                toPush.IsActive = source[csventry]["IsActive"];
                toPush.StartDate = source[csventry]["StartDate"];
                toPush.EndDate = source[csventry]["EndDate"];
            }
            else if(req.body.EntityTypeID==4){
                toPush.AccountingPackageID = source[csventry]["AccountingPackageID"];
                toPush.AllowIncompleteTimesheetSubmission = source[csventry]["AllowIncompleteTimesheetSubmission"];
                toPush.BillingRate = source[csventry]["BillingRate"];
                toPush.CostRate = source[csventry]["CostRate"];
                toPush.EmployeeNumber = source[csventry]["EmployeeNumber"];
                toPush.SecurityLevel = source[csventry]["SecurityLevel"];
                toPush.MinimumTimeHours = source[csventry]["MinimumTimeHours"];
                toPush.MinimumTimePeriod = source[csventry]["MinimumTimePeriod"];
                toPush.IsActive = source[csventry]["IsActive"];
                toPush.StartDate = source[csventry]["StartDate"];
                toPush.EndDate = source[csventry]["EndDate"];
            }
  
            //push the new data object into array of objects that will be POSTed to API
            objtoPOST.data.push(toPush);
        }

        //Call Post Allocations 
            const postresp = APIoperations.postAllocations(req.signedCookies.AuthTokenCookie,objtoPOST).then(function(response) {
                    // handle success
                    console.log("Successfull Allocations Post");
                    JobNumber = response.data.JobNumber;
                    console.log("Job Number after Allocations Post: "+JobNumber)
                    let recordsProcessed;

                    APIoperations.getJobDetailSummary(JobNumber,req.signedCookies.AuthTokenCookie).then(function(response2) {
                        console.log(response2.data)
                        recordsProcessed = response2.data["Total Records In Request"];

                    }).then(function(result) {

                        res.render('pages/UploadSuccess', {
                            uploadstatus: 'Upload in Progress',
                            recordsProcessedproperty: recordsProcessed,
                            JobID: JobNumber,
                        })
                    }).catch(function(error) {
                        // handle error
                        console.log("Error in getting Job Detail");
                        console.log(error);
                        res.send('Error in getting Job Detail Summary');
                    })
                })
                .catch(function(error) {
                    // handle error
                    console.log("Error in Allocations Post");
                    console.log(error);
                    res.render('pages/UploadFailure',{
                        global_token: req.signedCookies.AuthTokenCookie
                    }  );
                })

            fs.unlinkSync(req.file.path);
                
        }).catch(function(error) {
            // handle error
            console.log("Error Loading CSV File");
            console.log(error);
            res.render('pages/UploadFailure',{
                global_token: req.signedCookies.AuthTokenCookie
            } );
        })

  

})

//Load CSV of errors
app.post('/loaderrors/:jobnumber', (req, res) => {
    console.log("Requested Data " + JSON.stringify(req.params));
    var ObjForCSV = [];

    APIoperations.getJobDetail(req.params.jobnumber,req.signedCookies.AuthTokenCookie).then(function(response) {
            // handle success
            //console.log("Response.data: "+ JSON.stringify(response.data));
            const JobInfo = response.data;
            console.log("++++"+ req.params.jobnumber+"+++++"+JobInfo)
            Promise.all([APIoperations.getUserList(req.signedCookies.AuthTokenCookie), APIoperations.getProjectList(req.signedCookies.AuthTokenCookie)]).then((values) => {
                    const UserList = values[0].data.data;
                    const ProjectList = values[1].data.data;
                    for (var key in JobInfo) {
                        var ObjToPush = {};

                        for (var userkey in UserList) {
                            if (JobInfo[key].UserID == null) {
                                console.log("Record has no User ID");
                                ObjToPush.Message = "Employee Number not found"
                                break;
                            } else if (JobInfo[key].UserID == UserList[userkey].ID && JobInfo[key].UserID != null) {
                                console.log("Found matching UserID");
                                ObjToPush["Employee No"] = UserList[userkey].EmployeeNumber;
                                break;
                            }
                        }
                        ObjToPush["Effective Date"] = JobInfo[key].Month;
                        for (var projectkey in ProjectList) {
                            if (JobInfo[key].JobID == null) {
                                console.log("Record has no Project ID");
                                ObjToPush.Message = "Project not found";
                                break;
                            } else if (JobInfo[key].JobID == ProjectList[projectkey].ID && JobInfo[key].JobID != null) {
                                console.log("Found matching ProjectID");
                                ObjToPush["Speedkey Code"] = ProjectList[projectkey].AccountingPackageID;
                                break;
                            }
                        }
                        ObjToPush.Hours = JobInfo[key].EstHours;
                        
                        //Determine the success/errror Message
                        if (JobInfo[key].APIResponseCode == "201" ) {
                            ObjToPush.Response = "Success"
                        } else {
                            ObjToPush.Response = "Error";
                            if(JobInfo[key].ImportJobComments==null){
                                ObjToPush.Message = JobInfo[key].APIDetailResponse;
                            }    
                        }

                        ObjForCSV.push(ObjToPush);
                    }

                    csvFileName = "ErrorLog - Job " + req.params.jobnumber + ".csv"; //Include jobnumber in filename
                    res.set({
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=' + csvFileName
                    });

                    const csvContent = JSONToCSV(ObjForCSV);
                    res.send(csvContent);

                })
                .catch(function(error) {
                    // handle error
                    console.log(error)

                })

        })
        .catch(function(error) {
            // handle error
            console.log("Error in JobDetails Request")
            res.send("Job Number doesn't exist");

        })

});


module.exports = app;