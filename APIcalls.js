const axios = require('axios');

module.exports = {

    getLoginDetail(token) {
        
        var config = {
            headers: {
                'Authorization': token
            }
        };
        return axios.get('https://app.clicktime.com/rest/v2/Me', config);

    },

    postAllocations(cookietoken,objtoPOST) {

        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
    
        };
        // return axios.post('https://coredataloader-dev01.azurewebsites.net/api/ImportJob', objtoPOST, config);
        return axios.post('https://pscust2042458funcstaging3.azurewebsites.net/api/ImportJob', objtoPOST, config);
    
    },
    
    //api call to ClickTime return full list of users
    getUserList(cookietoken) {
    
        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
        };
        return axios.get('https://app.clicktime.com/rest/v2/Users?limit=3000', config);
    },
    
    //api call to ClickTime return projects list
    getProjectList(cookietoken) {
    
        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
        };
        return axios.get('https://app.clicktime.com/rest/v2/Jobs?CTLegacyScramble=true&limit=3000', config);
    },
    
    //api call to DataLoader Function for JobDetailSummary
    getJobDetailSummary(JobNumber,cookietoken) {
    
        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
        };
        return axios.get('https://pscust2042458funcstaging3.azurewebsites.net/api/ImportJobDetailSummary/' + JobNumber, config);
    },
    
    //api call to DataLoader Function for JobDetail
    getJobDetail(JobNumber,cookietoken) {
        console.log("Getting Job Details...")
        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
        };
        return axios.get('https://pscust2042458funcstaging3.azurewebsites.net/api/ImportJobDetail?jobid=' + JobNumber, config);
    },
    
    //api call to DataLoader Function for List of Jobs for current Token
    getJobList(cookietoken) {
    
        let currtoken = cookietoken;
        var config = {
            headers: {
                'Authorization': 'Token ' + currtoken,
                'Content-Type': 'application/json'
            }
        };
        return axios.get('https://pscust2042458funcstaging3.azurewebsites.net/api/JobsList', config);
    }
    
};
